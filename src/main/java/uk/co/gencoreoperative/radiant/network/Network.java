/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2014 ForgeRock AS.
 */
package uk.co.gencoreoperative.radiant.network;

import java.net.*;
import java.util.Enumeration;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Collection of Network based functions.
 */
public class Network {

    private final Set<InetAddress> addresses;

    public Network() {
        addresses = getAddresses(getAdapters());
    }

    /**
     * @return A set of applicable NetworkInterfaces
     */
    private Set<NetworkInterface> getAdapters() {
        try {
            Set<NetworkInterface> r = new HashSet<>();
            Enumeration<NetworkInterface> interfaces = NetworkInterface.getNetworkInterfaces();
            while (interfaces.hasMoreElements()) {
                NetworkInterface networkInterface = interfaces.nextElement();

                if (networkInterface.isVirtual()) continue;
                if (!networkInterface.isUp()) continue;
                if (networkInterface.isLoopback()) continue;
                r.add(networkInterface);
            }
            return r;
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    private Set<InetAddress> getAddresses(Set<NetworkInterface> interfaces) {
        Set<InetAddress> r = new HashSet<>();
        for (NetworkInterface networkInterface : interfaces) {
            List<InterfaceAddress> interfaceAddresses = networkInterface.getInterfaceAddresses();
            for (InterfaceAddress interfaceAddress : interfaceAddresses) {
                InetAddress address = interfaceAddress.getAddress();
                if (address instanceof Inet6Address) continue;
                r.add(address);
            }
        }
        return r;
    }

    /**
     * Returns a collection of all active addresses from the various
     * network adapters on the system.
     *
     * @return A possibly empty collection.
     */
    public Set<InetAddress> getAddresses() {
        return addresses;
    }

    /**
     * Indicates if the address provided is from one of the local adapters on this system.
     * @param address An InetAddress which may be remote.
     * @return True if the address is local.
     */
    public boolean isAddressFromLocalAdapter(InetAddress address) {
        for (InetAddress local : addresses) {
            if (local.getHostAddress().equals(address.getHostAddress())) {
                return true;
            }
        }
        return false;
    }
}
