/*
 * The contents of this file are subject to the terms of the Common Development and
 * Distribution License (the License). You may not use this file except in compliance with the
 * License.
 *
 * You can obtain a copy of the License at legal/CDDLv1.0.txt. See the License for the
 * specific language governing permission and limitations under the License.
 *
 * When distributing Covered Software, include this CDDL Header Notice in each file and include
 * the License file at legal/CDDLv1.0.txt. If applicable, add the following below the CDDL
 * Header, with the fields enclosed by brackets [] replaced by your own identifying
 * information: "Portions copyright [year] [name of copyright owner]".
 *
 * Copyright 2014 ForgeRock AS.
 */
package uk.co.gencoreoperative.radiant.network;

import uk.co.gencoreoperative.radiant.Config;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;

/**
 * Tracks sockets created and signals them to close when the application
 * is to shutdown.
 */
public class SocketFactory {
    private final int port;

    public SocketFactory(Config config) {
        this.port = config.getPort();
    }

    /**
     * Binds to the given address and port.
     *
     * @param address Address to bind to.
     * @return A DatagramSocket if the bind was successful.
     */
    public DatagramSocket getSocket(InetAddress address) {
        try {
            return new DatagramSocket(port, address);
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }

    /**
     * Binds to any address and port.
     *
     * @return A socket if the bind was successful.
     */
    public DatagramSocket getSocket() {
        try {
            return new DatagramSocket();
        } catch (SocketException e) {
            throw new RuntimeException(e);
        }
    }
}