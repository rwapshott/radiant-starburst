package uk.co.gencoreoperative.radiant.network;

import uk.co.gencoreoperative.radiant.Config;
import uk.co.gencoreoperative.radiant.Logger;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.SocketException;
import java.net.SocketTimeoutException;
import java.net.UnknownHostException;

/**
 * UDP Networking functions.
 */
public class UDP {
    private Integer packetSize;
    private Logger logger;
    private SocketFactory factory;
    private final int broadcastPort;
    private final boolean narrow;

    public UDP(Integer packetSize, Logger logger, SocketFactory factory, Config config) {
        this.packetSize = packetSize;
        this.logger = logger;
        this.factory = factory;
        broadcastPort = config.getPort();
        narrow = config.isNarrow();
    }

    /**
     * Performs a star burst style broadcast where by each possible ip address
     * is broadcast to on the local network.
     *
     * This method, whilst unpleasent demonstrably works in situations where
     * multi-cast would fail.
     *
     * address - The address to base the starburst on
     * post - The port to send to
     * message - The contents to transmit
     */
    public void starburst(InetAddress address, String message) {
        DatagramSocket socket = factory.getSocket();
        String[] b = address.getHostAddress().split("\\.");

        if (narrow) {
            narrow(socket, b, message);
        } else {
            wide(socket, b, message);
        }

        socket.close();
    }

    private void narrow(DatagramSocket socket, String[] address, String message) {
        for (int end = 1; end <= 255; end++) {
            address[3] = Integer.toString(end);
            send(socket, address, message);
        }
    }

    private void wide(DatagramSocket socket, String[] address, String message) {
        for (int c = 1; c <= 255; c++) {
            for (int d = 1; d <= 255; d++) {
                address[2] = Integer.toString(c);
                address[3] = Integer.toString(d);
                send(socket, address, message);
            }
        }
    }

    private void send(DatagramSocket socket, String[] target, String message) {
        byte[] buf = new byte[packetSize];
        byte[] msgBytes = message.getBytes();
        System.arraycopy(msgBytes, 0, buf, 0, Math.min(msgBytes.length, buf.length));
        DatagramPacket packet = new DatagramPacket(buf, buf.length, getInetAddress(target), broadcastPort);

        String msg = packet.getAddress().getHostAddress() + ":" + packet.getPort();

        try {
            socket.send(packet);
            logger.debug(msg);
        } catch (IOException e) {
            logger.debug(msg + " " + e.getMessage());
        }
    }

    /**
     * Performs a read on a socket, for a short time period. This is
     * a blocking call for a short duration (50ms?).
     */
    public DatagramPacket receive(DatagramSocket socket) {
        try {
            byte[] buf = new byte[packetSize];
            DatagramPacket packet = new DatagramPacket(buf, packetSize);

            try {
                socket.receive(packet);
                logger.debug("Received from: " + packet.getAddress().getHostAddress());
                logger.debug("Contents: " + new String(packet.getData()));
                return packet;
            } catch (SocketTimeoutException e) {
                logger.print("timeout");
                return null;
            } catch (SocketException e) {
                if (socket.isClosed()) return null;
                throw new IllegalStateException(e);
            }
        } catch (Throwable e) {
            logger.error(e);
        }
        return null;
    }

    private InetAddress getInetAddress(String[] b) {
        try {
            String address = "";
            for (String part : b) {
                address += part + ".";
            }
            address = address.substring(0, address.length() - 1);
            return InetAddress.getByName(address);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        }
    }
}
