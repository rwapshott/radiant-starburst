package uk.co.gencoreoperative.radiant;

import java.text.MessageFormat;

/**
 * As simple a logger as it gets.
 */
public class Logger {
    private final Config config;
    public Logger(Config config) {
        this.config = config;
    }

    public void print(String format, Object... args) {
        System.out.println(MessageFormat.format(format, args));
    }

    public void print(String messsage) {
        System.out.println(messsage);
    }

    public void debug(String message) {
        if (config.isDebug()) {
            System.out.println("D: " + message);
        }
    }
    public void debug(String format, Object... args) {
        if (config.isDebug()) {
            System.out.println(MessageFormat.format(format, args));
        }
    }

    public void error(String format, String... args) {
        System.err.println(MessageFormat.format(format, args));
    }

    public void error(Throwable e) {
        e.printStackTrace(System.out);
    }
}
