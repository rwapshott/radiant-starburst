package uk.co.gencoreoperative.radiant;

import com.beust.jcommander.Parameter;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Configuration for the application.
 */
public class Config {

    @Parameter (required = true, description = "hostname")
    private List<String> hostname = new ArrayList<String>();

    @Parameter (names = {"-t", "--times"}, description = "The duration in seconds to run the search for.")
    private int duration = 20;

    @Parameter (names = {"-d", "--debug"}, description = "Enables extra debugging output.")
    private boolean debug = false;

    @Parameter (names = {"-h", "--help"}, help = true, description = "This help information.")
    private boolean help = false;

    @Parameter (names ={ "-p", "--port"}, description = "The port number of broadcast on.")
    private int port = 4444;

    @Parameter (names ={"-n", "--narrow"}, description = "Perform a narrower broadcast only to local subnet", required = false)
    private boolean narrow = false;

    private final File hosts = getEtcFile();

    /**
     * The mandatory hostname of the application.
     *
     * @return Non null.
     */
    public String getHostname() {
        if (hostname.size() != 1) {
            throw new IllegalStateException();
        }
        return hostname.get(0);
    }

    /**
     * Duration of how long to run the discovery for.
     * @return Seconds.
     */
    public int getDuration() {
        return duration;
    }

    /**
     * @return True if debug mode has been enabled.
     */
    public boolean isDebug() {
        return debug;
    }

    /**
     * @return The port number to use for broadcast.
     */
    public int getPort() {
        return port;
    }

    /**
     * @return True if the help flag has been set.
     */
    public boolean isHelp() {
        return help;
    }

    /**
     * @return True if a narrow broadcast is required.
     */
    public boolean isNarrow() {
        return narrow;
    }

    /**
     * @return The system hosts file. May not exist, may not be writable.
     */
    public File getHosts() {
        return hosts;
    }

    public static File getEtcFile() {
        File folder;
        if (System.getProperty("os.name").toLowerCase().equals("windows")) {
            folder = new File("\\WINDOWS\\SYSTEM32\\drivers\\etc");
        } else {
            folder = new File("/etc");
        }
        return new File(folder, "hosts");
    }
}
