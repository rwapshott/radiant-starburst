package uk.co.gencoreoperative.radiant;

import com.beust.jcommander.JCommander;
import com.beust.jcommander.ParameterException;
import uk.co.gencoreoperative.radiant.util.Files;

/**
 * Responsible for launching the application and managing
 * application life cycle.
 */
public class Main {

    public static final String PROGRAM_NAME = "radient";
    private static Files files;

    public static void main(String... args) {
        Config config = new Config();
        files = new Files();

        JCommander commander = new JCommander(config);
        commander.setProgramName(PROGRAM_NAME);

        try {
            commander.parse(args);
        } catch (ParameterException e) {
            usageAndExit(commander);
        }

        if (config.isHelp()) {
            // Read usage document and print.
            for (String s : files.readLines(Main.class.getResourceAsStream("/usage"))) {
                System.out.println(s);
            }
            System.out.println("");

            // Print standout help output
            usageAndExit(commander);
        }

        new RadientStarburst(config).start();
    }

    private static String printVersion() {
        for (String s : files.readLines(Main.class.getResourceAsStream("/pom.xml"))) {
            if (s.contains("<version")) {
                String [] parts = s.split("[<>]");
                return parts[2];
            }
        }
        throw new IllegalStateException("Could not find version in pom.xml");
    }

    private static void usageAndExit(JCommander commander) {
        System.out.println("Version: " + printVersion());
        System.out.println("");
        commander.usage();
        System.exit(0);
    }
}
