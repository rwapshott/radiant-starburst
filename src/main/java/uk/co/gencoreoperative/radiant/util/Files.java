package uk.co.gencoreoperative.radiant.util;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.nio.charset.Charset;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

/**
 * File based utilities.
 */
public class Files {

    /**
     * Read the file as text, line by line.
     * @param file Non null file to read.
     * @return A non null array of zero or more lines of text.
     */
    public List<String> readLines(File file) {
        try {
            return java.nio.file.Files.readAllLines(
                    file.toPath(),
                    Charset.defaultCharset());
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }

    /**
     * Simple read all lines from stream function.
     * @param stream Non null, open stream.
     * @return A non null, possibly empty list.
     */
    public List<String> readLines(InputStream stream) {
        List<String> r = new ArrayList<>();
        try (BufferedReader reader = new BufferedReader(new InputStreamReader(stream));) {
            String data = "";
            while (data != null) {
                data = reader.readLine();
                if (data == null) continue;
                r.add(data);
            }
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
        return r;
    }

    /**
     * Write the lines of text to a file. Will create the file if it is
     * missing. Will overwrite the contents if present.
     *
     * @param lines Non null lines of text to write
     * @param file Non null file to write to.
     */
    public void writeLines(List<String> lines, File file) {
        try {
            java.nio.file.Files.write(
                    file.toPath(),
                    lines,
                    Charset.defaultCharset(),
                    StandardOpenOption.TRUNCATE_EXISTING);
        } catch (IOException e) {
            throw new IllegalStateException(e);
        }
    }
}
