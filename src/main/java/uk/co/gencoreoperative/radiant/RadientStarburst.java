package uk.co.gencoreoperative.radiant;

import uk.co.gencoreoperative.frp.SignalFactory;
import uk.co.gencoreoperative.frp.SignalListener;
import uk.co.gencoreoperative.frp.Signals;
import uk.co.gencoreoperative.radiant.util.Files;
import uk.co.gencoreoperative.radiant.network.Network;
import uk.co.gencoreoperative.radiant.network.SocketFactory;
import uk.co.gencoreoperative.radiant.network.UDP;

import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.InetAddress;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Launches the application.
 */
public class RadientStarburst {
    private final Files files = new Files();
    private final Network network = new Network();
    private final Signals signals = new Signals();

    private final Logger logger;
    private final SocketFactory socketFactory;
    private final UDP udp;

    private final Config config;

    private final Map<String, String> receivedHosts = new ConcurrentHashMap<>();

    public RadientStarburst(Config config) {

        this.config = config;
        logger = new Logger(this.config);
        socketFactory = new SocketFactory(this.config);
        udp = new UDP(100, logger, socketFactory, this.config);
        register();
    }

    private void register() {
        /**
         * During shutdown, we will also signal Signals to shutdown.
         */
        signals.register(Shutdown.class, new SignalListener<Shutdown>() {
            public void process(Shutdown shutdown, Signals signals) {
                signals.shutdownWhenComplete();
            }
        });

        /**
         * For each InetAddress (Network Adapter) signal a repeating Broadcast once a
         * second, for configured duration seconds.
         */
        signals.register(InetAddress.class, new SignalListener<InetAddress>() {
            public void process(InetAddress inetAddress, Signals signals) {
                SignalFactory factory = new SignalFactory(signals);
                Broadcast signal = new Broadcast(inetAddress, config.getHostname());
                factory.repeats(1000, config.getDuration(), signal);
            }
        });

        /**
         * When a broadcast signal is received, perform a UDP starburst using the InetAddress
         * and hostname provided.
         */
        signals.register(Broadcast.class, new SignalListener<Broadcast>() {
            public void process(Broadcast b, Signals signals) {
                logger.print("broadcasting {0} on {1}", b.message, b.address.getHostAddress());
                udp.starburst(b.address, b.message);
            }
        });

        /**
         * For each InetAddress (Network Adapter) detected, setup a UDP receiver for
         * incoming packets on that adapter.
         */
        signals.register(InetAddress.class, new SignalListener<InetAddress>() {
            @Override
            public void process(InetAddress address, Signals signals) {
                signals.add(new Listener(socketFactory, address, udp, signals, network));
            }
        });

        /**
         * When a UDP packet is received, put it in a map of received signals from other
         * systems.
         */
        signals.register(Received.class, new SignalListener<Received>() {
            public void process(Received received, Signals signals) {
                if (!receivedHosts.containsKey(received.host)) {
                    receivedHosts.put(received.host, received.addresss);
                    logger.print("Adding: {0} - {1}", received.addresss, received.host);
                }
            }
        });

        /**
         * Once the number of Broadcasts have been signalled, read the hosts file
         * and signal each host entry.
         */
        signals.register(Broadcast.class, new SignalListener<Broadcast>() {
            int total = network.getAddresses().size() * config.getDuration();
            int count = 0;
            @Override
            public void process(Broadcast broadcast, Signals signals) {
                count++;
                if (count != total) return;

                // To simplify output
                try {
                    Thread.sleep(500);
                } catch (InterruptedException e) {
                    throw new IllegalStateException(e);
                }

                /**
                 * Final sequence will be to modify the /etc/hosts file with
                 * the received host information from other systems. Once
                 * complete we must send the Shutdown signal to terminate
                 * listener thread and FRPL Signals class.
                 */
                try {
                    // If we can't read the hosts file, there is nothing to be done.
                    if (!config.getHosts().canRead()) {
                        logger.error("Unable to read from {0}", config.getHosts().getPath());
                        return;
                    }

                    List<String> hostFileCopy = new ArrayList<>();

                    // Copy all lines, excluding references to received hosts
                    for (String hostFileLine : files.readLines(config.getHosts())) {
                        boolean skip = false;
                        for (String receivedHost : receivedHosts.keySet()) {
                            if (hostFileLine.contains(receivedHost)) {
                                skip = true;
                                break;
                            }
                        }
                        if (!skip) {
                            hostFileCopy.add(hostFileLine);
                        }
                    }

                    // Add on formatted line for each received host
                    for (String hostname : receivedHosts.keySet()) {
                        hostFileCopy.add(receivedHosts.get(hostname) + " " + hostname);
                    }

                    // If we can't write, just indicate intention.
                    if (!config.getHosts().canWrite()) {
                        logger.print("Cannot write to {0}, printing contents instead:",
                                config.getHosts().getPath());

                        for (String line : hostFileCopy) {
                            logger.print(line);
                        }
                    } else {
                        logger.print("Writing {0} received hosts to {1}",
                                receivedHosts.size(), config.getHosts().getPath());
                        files.writeLines(hostFileCopy, config.getHosts());
                    }
                } finally {
                    signals.add(new Shutdown());
                }
            }
        });
    }

    /**
     * Start the application logic by enumerating the available network
     * adapters and passing them into Signals.
     */
    public void start() {
        for (InetAddress address : network.getAddresses()) {
            signals.add(address);
        }

        /**
         * If there is only one address, then we will also assume that
         * the user wants this added to their hosts.
         */
        if (network.getAddresses().size() == 1) {
            Received thisAddress = new Received(
                    config.getHostname(),
                    network.getAddresses().iterator().next().getHostAddress());
            signals.add(thisAddress);
        }
    }


    private static class Broadcast {
        InetAddress address;
        String message;
        public Broadcast(InetAddress address, String message) {
            this.address = address;
            this.message = message;
        }
    }

    private static class Received {
        private String host;
        private String addresss;

        private Received(String host, String addresss) {
            this.host = host;
            this.addresss = addresss;
        }
    }

    private static class Shutdown {}

    /**
     * Responsible for listening to packets arriving on a UDP socket,
     * forwarding them on for signalling.
     *
     * Importantly with UDP Receive, the receiver needs to be blocking on receive
     * at the time when the packet arrives, otherwise the network adapter will
     * discard the packet.
     */
    private static class Listener implements Runnable {

        private final UDP udp;
        private final Signals signals;
        private final Network network;
        private final DatagramSocket socket;

        private Listener(SocketFactory socketFactory, InetAddress address, UDP udp, Signals signals, Network network) {
            this.udp = udp;
            this.signals = signals;
            this.network = network;
            socket = socketFactory.getSocket(address);

            signals.register(Shutdown.class, new SignalListener<Shutdown>() {
                @Override
                public void process(Shutdown shutdown, Signals signals) {
                    socket.close();
                }
            });
        }

        @Override
        public void run() {
            while (!socket.isClosed()) {
                DatagramPacket packet = udp.receive(socket);
                if (packet == null) continue;
                if (network.isAddressFromLocalAdapter(packet.getAddress())) continue;

                String hostname = new String(packet.getData()).trim();
                String target = packet.getAddress().getHostAddress();

                signals.add(new Received(hostname, target));
            }
        }
    }
}
